﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour 
{
	public LayerMask groundLayers;
	public float speed;
	public float jumpForce;

	private Rigidbody2D body;

	private float moveDirection;
	private bool jump;

	// Use this for initialization
	void Awake () 
	{
		body = GetComponent<Rigidbody2D>();
	}
		
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		moveDirection = Input.GetAxis("Horizontal")*-1;

		if(Input.GetButtonDown("Jump"))
		{
			jump = true;
		}

	}

	// Update is called once per physics interation
	void FixedUpdate () 
	{
		bool isOnGround = Physics2D.Linecast(transform.position, transform.position + new Vector3(0f,-0.8f,0), groundLayers);

		//body.AddForce(new Vector2(moveDirection*speed, 0f));
		body.AddTorque(moveDirection*speed);

		if(jump) 
		{
			if (isOnGround) 
			{
				body.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
			}
			jump = false;
		}
	}

}
