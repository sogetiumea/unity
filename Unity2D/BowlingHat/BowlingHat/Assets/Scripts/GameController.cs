﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour 
{
	public static GameController instance;

	void Awake () 
	{
		if (instance == null) 
		{
			instance = this;
			DontDestroyOnLoad(this);
		}
		else 
		{
			Destroy(gameObject);
		}
	}

	public void RestartLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
		
	public void NextLevel()
	{
		var nextLevel = SceneManager.GetActiveScene().buildIndex+1;
		if (nextLevel >= SceneManager.sceneCountInBuildSettings) 
		{
			nextLevel = 0;
		}
		SceneManager.LoadScene(nextLevel);
	}

}

